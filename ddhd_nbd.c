#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <orca/discord.h>

#include "util/ryuutil.h"
#include "ddhd_discord.h"

#define NBDKIT_API_VERSION 2
#include <nbdkit-plugin.h>
#define THREAD_MODEL NBDKIT_THREAD_MODEL_SERIALIZE_ALL_REQUESTS
// Data

block_device * disk;
discord_cache cache;

// Stuff

int ddhd_true(void * h) {return 1;} 
int ddhd_false(void * h) {return 0;} 

void ddhd_load()
{
	disk = (block_device *)malloc(sizeof(block_device));
	disk->disk_size = 48 * 1024;
	disk->raw_data = (uint8_t *)malloc(sizeof(uint8_t)*disk->disk_size);

	cache = discord_cache_allocate(disk->disk_size / ALLOCATION_UNIT, 963097940325453865);
}

void on_ready(struct discord * client) {
	fprintf(stderr, "Logged in as %s!", discord_get_self(client)->username);
}

void * ddhd_open(int ro)
{
	cache.client = discord_init(cache.token);
	discord_set_on_ready(cache.client , &on_ready);
	format_channel(&cache);

	return disk;
}

void ddhd_close(void * h)
{
	discord_shutdown(cache.client);
}

void ddhd_unload()
{
	discord_cache_free(&cache);
	free(disk->raw_data);
	free(disk);
}

int64_t ddhd_get_size(void * h)
{
	block_device * drive = (block_device *)h;
	return drive->disk_size;
}

int ddhd_block_size(void * h, uint32_t * min, uint32_t * pref, uint32_t * max)
{
	* min = 512;
	* pref = ALLOCATION_UNIT;
	* max = 4 * ALLOCATION_UNIT;
	return 1;
}

int ddhd_can_write(void * h)
{
	return true;
}

int ddhd_pwrite(void * h, const void * buf, uint32_t count, uint64_t offset, uint32_t flags)
{
	block_device * drive = (block_device *)h;
	uint8_t * ubuf8 = (uint8_t *)buf; 
	for (int32_t byte = 0; byte < count; byte++)
	{
		drive->raw_data[offset + byte] = ubuf8[byte];
	}
	int overflow = count % ALLOCATION_UNIT;
	int final = (offset / ALLOCATION_UNIT) + (count / ALLOCATION_UNIT) + (overflow ? 1 : 0);
	for (int page = offset / ALLOCATION_UNIT; page < final; page++)
	{
		discord_update_page(drive, cache, page);
	}
	return 0;
}

int ddhd_flush(void * h, uint32_t flags)
{
	block_device * drive = (block_device *)h;

	for (int page = 0; page < cache.num_pages; page++)
	{
		discord_update_page(drive, cache, page);
	}

	return 0;
}

void ddhd_load_pages(int start, int count)
{
	for (int page = start; page < (start + count); page++)
	{
		discord_load_page(disk, cache, page);
	}
}

int ddhd_can_cache(void * h)
{
	return NBDKIT_CACHE_NATIVE;
}

int ddhd_cache(void * h, uint32_t count, uint64_t offset, uint32_t flags)
{
	int overflow = count % ALLOCATION_UNIT;
	ddhd_load_pages(offset / ALLOCATION_UNIT, (count / ALLOCATION_UNIT) + (overflow ? 1 : 0));
	return 0;
}

int ddhd_pread(void * h, void * buf, uint32_t count, uint64_t offset, uint32_t flags)
{
	block_device * drive = (block_device *)h;


	int overflow = count % ALLOCATION_UNIT;
	ddhd_load_pages(offset / ALLOCATION_UNIT, count/ ALLOCATION_UNIT + (overflow ? 1 : 0));

	uint8_t * ubuf8 = (uint8_t *)buf; 
	for (uint32_t byte = 0; byte < count; byte++)
	{
		ubuf8[byte] = drive->raw_data[offset + byte];
	}

	return 0;
}



static struct nbdkit_plugin plugin = {
	.name			= "DDHD",
	.version		= "1.0",
	.longname		= "Discord Data Harder Drive",
	.description 	= "Messages go brrr",
	.load			= ddhd_load,
	.unload			= ddhd_unload,
	.open			= ddhd_open,
	.close			= ddhd_close,
	.get_size		= ddhd_get_size,
	//.block_size		= ddhd_block_size,
	.can_write		= ddhd_can_write,
	.can_flush		= ddhd_true,
	.is_rotational	= ddhd_true,
	.can_multi_conn = ddhd_false,
	.can_cache		= ddhd_can_cache,
	.pread			= ddhd_pread,
	.pwrite			= ddhd_pwrite,
	.flush			= ddhd_flush,
	.cache			= ddhd_cache,
};
NBDKIT_REGISTER_PLUGIN(plugin);