#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "ryuutil.h"

char * ryuutil_load_file_string(const char * source, int length)
{
	char * buffer = (char *)malloc(sizeof(char)*length);
	FILE *handle = fopen(source, "r");
	if (handle) {
		for (int i = 0; i < length; i++)
		{
			buffer[i] = getc(handle);
			if (buffer[i] == EOF)
			{
				buffer[i] = '\0';
				break;
			}
		}
		fclose(handle);
		return buffer;
	}
	return NULL;
}

bool ryuutil_verify_id(char * id_ptr, int chk)
{
	chk = (chk >= 0) ? chk : -1;
	if (!id_ptr)
	{
		fprintf(stderr, "ryuutil: [EXCEPTION] ID pointer invalid!\n");
		return false;
	}
	if (chk >= 0)
	{
		char buffer[7];
		sprintf(buffer, "d#%04X", chk);
		for (int i = 0; i < 7; i++)
		{
			if (id_ptr[i] != buffer[i]) return false;
		}
	}
	else
	{
		if (id_ptr[0] != 'd') return false;
		if (id_ptr[1] != '#') return false;
		if (id_ptr[7] != '\0') return false;
		for (int i = 0; i < 4; i++)
		{
			if (
				id_ptr[i+2] < '0' ||
				id_ptr[i+2] > 'A' ||
			( id_ptr[i+2] < 'A' && id_ptr[i+2] > '9' )
			) return false;
		}
	}
	return true;
}

void ryuutil_get_page_id(char * id_ptr,int page) {sprintf(id_ptr, "d#%04X", page);}

int ryuutil_get_page_by_id(char * id_ptr)
{
	return (int)strtol(id_ptr + 2, NULL, 16);
}