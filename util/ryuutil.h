#pragma once
#include <stdbool.h>

char * ryuutil_load_file_string(const char *, int);

bool ryuutil_verify_id(char *, int);
void ryuutil_get_page_id(char *, int);
int ryuutil_get_page_by_id(char *);