#include "ddhd_discord.h"
#include "util/ryuutil.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

void on_ready(struct discord * client) {
	fprintf(stderr, "Logged in as %s!", discord_get_self(client)->username);
}

int main()
{
	block_device disk = {
		.disk_size = 8 * 1024,
		.raw_data = (uint8_t *)malloc(sizeof(uint8_t)*disk.disk_size)
	};

	char * channel_token = ryuutil_load_file_string("channel", 18);
	discord_cache cache = discord_cache_allocate(disk.disk_size / ALLOCATION_UNIT, (u64_snowflake_t)atol(channel_token));

	cache.client = discord_init(cache.token);
	discord_set_on_ready(cache.client, &on_ready);
	format_channel(&cache);

	memset(disk.raw_data, 10, sizeof(uint8_t)*disk.disk_size);
	discord_update_page(&disk, cache, 1);
	memset(disk.raw_data, 255, sizeof(uint8_t)*disk.disk_size);
	discord_update_page(&disk, cache, 2);
	memset(disk.raw_data, 11, sizeof(uint8_t)*disk.disk_size);
	discord_update_page(&disk, cache, 3);

	memset(disk.raw_data, 0, sizeof(uint8_t)*disk.disk_size);
	for (int i = 0; i < cache.num_pages; i++) {discord_load_page(&disk, cache, i);}

	return 0;
}