#pragma once
#define ALLOCATION_UNIT 2048
#include <orca/discord.h>
#include <stdbool.h>
#include <stdint.h>

typedef struct block_device {
	uint64_t disk_size;
	uint8_t * raw_data;
} block_device;

typedef struct discord_cache {
	u64_snowflake_t channel_id;
	int32_t num_pages;
	u64_snowflake_t * page_list;
	char * token;
	struct discord * client;
} discord_cache;

void format_channel(discord_cache * cache);

void discord_update_page(block_device * disk, discord_cache cache, int32_t page);
void discord_load_page(block_device * disk, discord_cache cache, int32_t page);

discord_cache discord_cache_allocate(int32_t num_pages, u64_snowflake_t channel_id);
void discord_cache_free(discord_cache * cache);