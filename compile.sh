#!/bin/sh
if [[ $1 == "standalone" ]]
then
	gcc -g3 -Iutil ddhd_main.c ddhd_discord.c util/ryuutil.c -o ddhd_standalone -ldiscord -lcurl -DDDHD_STANDALONE
else
	gcc -fPIC -g3 -Iutil -shared ddhd_nbd.c ddhd_discord.c util/ryuutil.c -o ddhd.so -ldiscord -lcurl
fi
