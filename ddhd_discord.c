#include "ddhd_discord.h"
#include "util/ryuutil.h"
#include <orca/discord.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <alloca.h>

void format_channel(discord_cache * cache)
{
	struct discord_message ** list;
	struct discord_get_channel_messages_params params = {};
	discord_get_channel_messages(cache->client, cache->channel_id, &params, &list);
	fprintf(stderr, "%d\n", cache->num_pages);
	for (int page_index = 0; page_index < cache->num_pages; page_index++) {
		if (list)
		{
			for (int msg = 0; list[msg]; msg++)
			{
				if (!list[msg]->embeds) continue;
				if (ryuutil_verify_id(list[msg]->embeds[0]->title, page_index))
				{
					cache->page_list[page_index] = list[msg]->id;
					goto _0000_SKIP;
				}
			}
		}
		if (false) _0000_SKIP:continue;
		uint8_t * zero = (uint8_t *)malloc(sizeof(uint8_t)*ALLOCATION_UNIT*2 +1);
		for (int i = 0; i < ALLOCATION_UNIT*2; i++) zero[i] = '0';
		zero[ALLOCATION_UNIT*2] = 0;
		struct discord_embed embed = {
			.description = zero
		};
		char id[8];
		ryuutil_get_page_id(id, page_index);
		discord_embed_set_title(&embed, id);
		struct discord_embed ** embed_list = (struct discord_embed **)malloc(sizeof(struct discord_embed *) * 2);
		embed_list[0] = &embed; embed_list[1] = NULL;
		struct discord_create_message_params msg_params = {
			.embeds = embed_list
		};
		struct discord_message * ret = (struct discord_message *)calloc(1, sizeof(struct discord_message));
		ORCAcode status = discord_create_message(cache->client, cache->channel_id, &msg_params, ret);
		if (status != 0) {fprintf(stderr, orca_strerror(status));}
		cache->page_list[page_index] = ret->id;
		free(zero);
		discord_message_cleanup(ret);
	}	
}

void discord_update_page(block_device * disk, discord_cache cache, int32_t page)
{
	uint8_t buffer[ALLOCATION_UNIT*2+1];
	uint64_t offset = ALLOCATION_UNIT*page;
	for (uint64_t i = 0; i < ALLOCATION_UNIT; i++)
	{
		char tmp[3];
		snprintf(tmp, 3, "%02X", disk->raw_data[i+offset]);
		buffer[i*2+0] = tmp[0]; buffer[i*2+1] = tmp[1];
	}
	buffer[ALLOCATION_UNIT*2] = '\0';
	struct discord_embed embed = {
		.description = buffer
	};
	char page_id[8];
	ryuutil_get_page_id(page_id, page);
	discord_embed_set_title(&embed, page_id);
	struct discord_embed ** embed_list = (struct discord_embed **)alloca(sizeof(struct discord_embed *)*2);
	embed_list[0] = &embed; embed_list[1] = NULL;
	struct discord_edit_message_params msg_params = {
		.embeds = embed_list
	};
	discord_edit_message(cache.client, cache.channel_id, cache.page_list[page], &msg_params, NULL);
}

void discord_load_page(block_device * disk, discord_cache cache, int32_t page)
{
	uint8_t buffer[ALLOCATION_UNIT];
	uint64_t offset = ALLOCATION_UNIT*page;
	struct discord_message message = {};
	discord_get_channel_message(cache.client, cache.channel_id, cache.page_list[page], &message);
	for (int byte = 0; byte < ALLOCATION_UNIT; byte++)
	{
		char tmp[3] = {message.embeds[0]->description[byte*2], message.embeds[0]->description[byte*2+1], '\0'};
		buffer[byte] = (uint8_t)strtoul(tmp, NULL, 16);
	}
	for (int i = 0; i < ALLOCATION_UNIT; i++) {disk->raw_data[offset+i] = buffer[i];}
}

discord_cache discord_cache_allocate(int32_t num_pages, u64_snowflake_t channel_id)
{
	discord_cache cache = {};
	cache.channel_id = channel_id;
	cache.num_pages = num_pages;
	cache.page_list = (u64_snowflake_t *)malloc(sizeof(u64_snowflake_t)*num_pages);
	cache.token = ryuutil_load_file_string("token", 60);


	return cache;
}

void discord_cache_free(discord_cache * cache)
{
	free(cache->page_list);
	cache->page_list = NULL;
}