# Discord Data Harder Drive
DDHD is a C NBDkit plugin that follows the idea of a "[Harder Drive](https://www.youtube.com/watch?v=JcJSW7Rprio)".\
Using a Discord bot, it stores data in discord messages within embeds, as if they were a hard drive platter.

## Usage
Place your bot token in a file called ``token``.\
Place the channel snowflake you wish to use in a file called ``channel``.\
Working settings are hard coded for now.

Run ``compile.sh`` in order to compile the binary.\
Run the resultant binary to start the plugin. Make sure that the ``nbd`` kernel module is loaded.\
Connect to the server with an appropriate program. The plugin will then format its data storage before becoming usable.
